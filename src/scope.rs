/*!
Build and manipulate scopes
*/

use super::*;
use rain_ast::ast::{ParamArgs, Scope};

impl<'a, S: Hash + Eq + Borrow<str> + From<&'a str>, B: BuildHasher> Builder<S, B> {
    /// Build a scope
    pub fn build_scope(&mut self, scope: &Scope<'a>) -> Result<ValId, Error<'a>> {
        self.push_scope();
        for statement in scope.statements.iter() {
            self.build_statement(statement)?
        }
        let result = scope.retv.as_ref().map(|expr| self.build_expr(expr));
        self.pop_scope();
        if let Some(result) = result {
            result
        } else {
            Err(Error::NotImplemented("Non-value scopes"))
        }
    }

    /// Build a set of parameter arguments into a region, registering a new scope for them
    /// Push this region onto the region stack
    pub fn push_args(&mut self, args: &ParamArgs<'a>) -> Result<(), Error<'a>> {
        //TODO: more efficiency for type vector construction...
        let tys: Result<Vec<_>, _> = args.iter().map(|(_, ty)| self.build_ty(ty)).collect();
        let region = Region::with(
            tys?.into_iter().collect(),
            self.stack
                .last()
                .map(|(r, _)| r.clone())
                .unwrap_or_default(),
        )?;
        self.push_scope();
        for (i, (id, _)) in args.iter().enumerate() {
            match id.get_sym() {
                Ok(Some(sym)) => self.symbols.insert(
                    sym.into(),
                    region
                        .clone()
                        .param(i)
                        .expect("Index must be in bounds")
                        .into(),
                ),
                Ok(None) => {}
                Err(_) => {
                    self.pop_scope();
                    return Err(Error::Message("Cannot assign to this symbol!"));
                }
            };
        }
        self.push_region(region);
        Ok(())
    }

    /// Push a region onto the region stack *without affecting the symbol table*
    pub fn push_region(&mut self, region: Region) {
        self.stack.push((region, self.symbols.depth()))
    }

    /// Pop the top region from the region stack, along with any scopes in the region. Return it, if any
    pub fn pop_region(&mut self) -> Option<Region> {
        if let Some((region, depth)) = self.stack.pop() {
            while self.symbols.depth() > depth {
                self.symbols.pop();
            }
            Some(region)
        } else {
            None
        }
    }

    /// Push a scope onto the symbol table
    pub fn push_scope(&mut self) {
        self.symbols.push()
    }

    /// Pop a scope from the symbol table
    pub fn pop_scope(&mut self) {
        self.symbols.pop()
    }
}
