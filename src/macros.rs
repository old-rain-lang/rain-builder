/*!
Build macro-like objects (e.g. judgemental equality)
*/

use super::*;
use rain_ast::ast::{Jeq, Let, TypeOf};

impl<'a, S: Hash + Eq + Borrow<str> + From<&'a str>, B: BuildHasher> Builder<S, B> {
    /// Build a let-statement
    pub fn build_let(&mut self, l: &Let<'a>) -> Result<(), Error<'a>> {
        let rhs = self.build_expr(&l.rhs)?;
        self.build_assign(&l.lhs, rhs)
    }

    /// Build a `rain` typeof expression
    pub fn build_typeof(&mut self, ty: &TypeOf<'a>) -> Result<TypeId, Error<'a>> {
        let expr = self.build_expr(&ty.0)?;
        Ok(expr.ty().clone_ty())
    }

    /// Build a judgemental equality comparison between a set of `rain` types, returning whether they are all equal
    pub fn build_jeq(&mut self, j: &Jeq<'a>) -> Result<bool, Error<'a>> {
        let mut iter = j.iter().map(|e| self.build_expr(e));
        let mut result = true;
        if let Some(first) = iter.next() {
            let first = first?;
            for item in iter {
                if first != item? {
                    result = false;
                }
            }
        }
        Ok(result)
    }
}
