/*!
Build S-expressions
*/

use super::*;
use rain_ast::ast::Sexpr as SExpr;
use rain_ir::value::expr::Sexpr;

impl<'a, S: Hash + Eq + Borrow<str> + From<&'a str>, B: BuildHasher> Builder<S, B> {
    /// Build an S-expression
    pub fn build_sexpr(&mut self, sexpr: &SExpr<'a>) -> Result<Sexpr, Error<'a>> {
        let args: Result<_, _> = sexpr.iter().map(|arg| self.build_expr(arg)).collect();
        Ok(Sexpr::try_new(args?)?)
    }
}
