/*!
Build gamma nodes
*/

use super::*;
use rain_ast::ast::{Branch, Expr, Finite as FiniteExpr, Gamma as GammaExpr, Pattern};
use rain_ir::control::ternary::Ternary;

impl<'a, S: Hash + Eq + Borrow<str> + From<&'a str>, B: BuildHasher> Builder<S, B> {
    /// Build a gamma node
    pub fn build_gamma(&mut self, gamma: &GammaExpr<'a>) -> Result<ValId, Error<'a>> {
        if gamma.params.is_empty() {
            return Err(Error::Message(
                "Gamma type must have at least one parameter!",
            ));
        }
        if gamma.params.len() != 1 {
            return Err(Error::Message(
                "Multi argument patterns are not currently supported!",
            ));
        }
        let param = &gamma.params[0];
        match param {
            Expr::BoolTy(_) => self
                .build_ternary_conditional(&gamma.branches)
                .map(ValId::from),
            Expr::Finite(f) if *f == FiniteExpr(2) => {
                self.build_ternary_switch(&gamma.branches).map(ValId::from)
            }
            _ => Err(Error::Message("Invalid gamma argument")),
        }
    }

    /// Build a conditional expression from a set of branches
    pub fn build_ternary_conditional(
        &mut self,
        branches: &[Branch<'a>],
    ) -> Result<Ternary, Error<'a>> {
        if branches.len() < 2 {
            return Err(Error::Message("Too few branches in boolean gamma node!"));
        }
        if branches.len() > 2 {
            return Err(Error::Message("Too many branches in boolean gamma node!"));
        }
        let first_branch_dest = match branches[0].pattern {
            Pattern::Bool(b) => b,
            Pattern::Index(_) => {
                return Err(Error::Message(
                    "Index pattern for first branch of boolean gamma node!",
                ))
            }
        };
        let second_branch_dest = match branches[1].pattern {
            Pattern::Bool(b) => b,
            Pattern::Index(_) => {
                return Err(Error::Message(
                    "Index pattern for second branch of boolean gamma node!",
                ))
            }
        };
        if first_branch_dest == second_branch_dest {
            return Err(Error::Message("Duplicate branch destinations"));
        }
        let first_branch = self.build_expr(&branches[0].expr)?;
        let second_branch = self.build_expr(&branches[1].expr)?;
        let (high, low) = if first_branch_dest {
            (first_branch, second_branch)
        } else {
            (second_branch, first_branch)
        };
        Ok(Ternary::conditional(high, low)?)
    }

    /// Build a ternary switch expression from a set of branches
    pub fn build_ternary_switch(&mut self, branches: &[Branch<'a>]) -> Result<Ternary, Error<'a>> {
        if branches.len() < 2 {
            return Err(Error::Message("Too few branches in ternary gamma node!"));
        }
        if branches.len() > 2 {
            return Err(Error::Message("Too many branches in ternary gamma node!"));
        }
        let first_branch_dest = match branches[0].pattern {
            Pattern::Index(i) if i.ty == Some(FiniteExpr(2)) => i.ix,
            Pattern::Index(_) => {
                return Err(Error::Message(
                    "Invalid index type for first branch of ternary gamma node",
                ))
            }
            Pattern::Bool(_) => {
                return Err(Error::Message(
                    "Boolean pattern for first branch of ternary gamma node!",
                ))
            }
        };
        let second_branch_dest = match branches[1].pattern {
            Pattern::Index(i) if i.ty == Some(FiniteExpr(2)) => i.ix,
            Pattern::Index(_) => {
                return Err(Error::Message(
                    "Invalid index type for second branch of ternary gamma node",
                ))
            }
            Pattern::Bool(_) => {
                return Err(Error::Message(
                    "Boolean pattern for second branch of ternary gamma node!",
                ))
            }
        };
        if first_branch_dest == second_branch_dest {
            return Err(Error::Message("Duplicate branch destinations"));
        }
        let first_branch = self.build_expr(&branches[0].expr)?;
        let second_branch = self.build_expr(&branches[1].expr)?;
        let (high, low) = if first_branch_dest != 0 {
            (first_branch, second_branch)
        } else {
            (second_branch, first_branch)
        };
        Ok(Ternary::switch(high, low)?)
    }
}
