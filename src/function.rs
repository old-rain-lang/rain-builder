/*!
Build functions and function types
*/

use super::*;
use rain_ast::ast::{Lambda as LambdaExpr, Parametrized as ParametrizedExpr, Pi as PiExpr};
use rain_ir::function::{lambda::Lambda, pi::Pi};
use rain_ir::region::Parametrized;

impl<'a, S: Hash + Eq + Borrow<str> + From<&'a str>, B: BuildHasher> Builder<S, B> {
    /// Build a lambda function
    pub fn build_lambda(&mut self, lambda: &LambdaExpr<'a>) -> Result<Lambda, Error<'a>> {
        self.build_parametrized(lambda).map(Lambda::new)
    }

    /// Build a pi type
    pub fn build_pi(&mut self, pi: &PiExpr<'a>) -> Result<Pi, Error<'a>> {
        let param = self
            .build_parametrized(pi)?
            .try_into_value()
            .map_err(|_| Error::Message("Pi type must parametrize a valid type"))?;
        let result = Pi::new(param)?;
        Ok(result)
    }

    /// Build a parametrized value
    pub fn build_parametrized(
        &mut self,
        param: &ParametrizedExpr<'a>,
    ) -> Result<Parametrized<ValId>, Error<'a>> {
        self.push_args(&param.args)?;
        let result = self.build_expr(&param.result);
        let region = self
            .pop_region()
            .expect("`push_args` should always push a region to the stack!");
        Parametrized::try_new(result?, region)
            .map_err(|err| Error::ValueError("Invalid parametrized value", err))
    }
}
