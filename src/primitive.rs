/*!
Build primitive types
*/

use super::*;
use rain_ast::ast::{Finite as FiniteExpr, Index as IndexExpr, Logical as LogicalExpr};
use rain_ir::primitive::{
    finite::{Finite, Index},
    logical::Logical,
};

impl<'a, S: Hash + Eq + Borrow<str> + From<&'a str>, B: BuildHasher> Builder<S, B> {
    /// Build a finite `rain` expression
    pub fn build_finite(&self, finite: FiniteExpr) -> Result<Finite, Error<'a>> {
        Ok(Finite(finite.0))
    }

    /// Build a logical `rain` expression
    pub fn build_logical(&self, logical: LogicalExpr) -> Result<Logical, Error<'a>> {
        Logical::try_new(logical.arity(), logical.data())
            .map_err(|_| Error::Message("Invalid logical expression!"))
    }

    /// Build an index into a finite type
    pub fn build_index(&self, ix: IndexExpr) -> Result<Index, Error<'a>> {
        if let Some(ty) = ix.ty {
            let ty = self.build_finite(ty)?;
            Index::try_new(ty, ix.ix).map_err(|_| Error::Message("Invalid index!"))
        } else {
            Err(Error::NotImplemented(
                "Index type inference not implemented!",
            ))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rain_ast::tokens::*;
    use rain_ir::prettyprinter::PrettyPrint;
    use rain_ir::value::Value;

    #[test]
    fn basic_indexing_works() {
        let mut builder = Builder::<&str>::new();
        let exprs: &[(&str, ValId)] = &[
            ("[#true #false ()] #ix(3)[1]", false.into()),
            ("[#false [#true] ()] #ix(3)[1] #ix(1)[0]", true.into()),
            ("[#false #finite(6) #false] #ix(3)[1]", Finite(6).into()),
        ];
        for (expr, value) in exprs {
            let (rest, expr) = builder.parse_expr(expr).expect(expr);
            assert_eq!(&expr, value);
            assert_eq!(rest, "");
        }
    }
    #[test]
    fn booleans_parse_properly() {
        let mut builder = Builder::<&str>::new();
        let f_true = format!("{}", true.prp());
        let f_false = format!("{}", false.prp());
        let f_bool = format!("{}", Bool);
        let f_bool_prp = format!("{}", Bool.prp());

        let (rest, expr) = builder.parse_expr(&f_true).expect(KEYWORD_TRUE);
        assert_eq!(rest, "");
        assert_eq!(expr, true.into_val());
        let (rest, expr) = builder.parse_expr(&KEYWORD_TRUE).expect(KEYWORD_TRUE);
        assert_eq!(rest, "");
        assert_eq!(expr, true.into_val());

        let (rest, expr) = builder.parse_expr(&f_false).expect(KEYWORD_FALSE);
        assert_eq!(rest, "");
        assert_eq!(expr, false.into_val());
        let (rest, expr) = builder.parse_expr(&KEYWORD_FALSE).expect(KEYWORD_FALSE);
        assert_eq!(rest, "");
        assert_eq!(expr, false.into_val());

        let (rest, expr) = builder.parse_expr(&f_bool).expect(KEYWORD_BOOL);
        assert_eq!(rest, "");
        assert_eq!(expr, Bool.into_val());
        let (rest, expr) = builder.parse_expr(&f_bool_prp).expect(KEYWORD_BOOL);
        assert_eq!(rest, "");
        assert_eq!(expr, Bool.into_val());
        let (rest, expr) = builder.parse_expr(&KEYWORD_BOOL).expect(KEYWORD_BOOL);
        assert_eq!(rest, "");
        assert_eq!(expr, Bool.into_val());

        assert!(builder.parse_expr("#fals").is_err());
    }

    fn test_binary_operation(
        op: Logical,
        partial_table: &[Logical; 2],
        truth_table: &[bool; 4],
        builder: &mut Builder<String>,
    ) {
        // Test parsing:
        assert_eq!(
            builder.parse_expr(&format!("{}", op)).unwrap(),
            ("", op.into())
        );
        assert_eq!(
            builder.parse_expr(&format!("{}", op.print_raw())).unwrap(),
            ("", op.into())
        );

        for left in [true, false].iter().copied() {
            for right in [true, false].iter().copied() {
                let ix = left as u8 | (right as u8) << 1;
                assert_eq!(op.get_bit(ix), truth_table[ix as usize]);
                let partial = op
                    .apply(left)
                    .right()
                    .expect("Expected binary operation, got unary!");
                assert_eq!(
                    partial, partial_table[left as usize],
                    "Incorrect partial evaluation of ({} {})",
                    op, left
                );
                assert_eq!(
                    builder.parse_expr(&format!("{} #{}", op, left)).unwrap(),
                    ("", partial.into())
                );
                assert_eq!(
                    builder.parse_expr(&format!("{}", partial)).unwrap(),
                    ("", partial.into())
                );
                assert_eq!(
                    builder
                        .parse_expr(&format!("{}", partial.print_raw()))
                        .unwrap(),
                    ("", partial.into())
                );
                let fin = partial
                    .apply(right)
                    .left()
                    .expect("Expected binary operation, got arity > 2!");
                assert_eq!(
                    fin, truth_table[ix as usize],
                    "Incorrect total evaluation of ({} {} {}) == ({} {})",
                    op, left, right, partial, right
                );
                assert_eq!(
                    builder
                        .parse_expr(&format!("{} #{} #{}", op, left, right))
                        .unwrap(),
                    ("", fin.into())
                );
                assert_eq!(
                    builder
                        .parse_expr(&format!("({} #{}) #{}", op, left, right))
                        .unwrap(),
                    ("", fin.into())
                );
                assert_eq!(
                    builder
                        .parse_expr(&format!("{} #{}", partial, right))
                        .unwrap(),
                    ("", fin.into())
                );
            }
        }
    }

    fn cl(b: bool) -> Logical {
        Logical::try_const(1, b).unwrap()
    }

    #[test]
    fn test_binary_operations() {
        use rain_ir::primitive::logical::Id;
        let mut builder = Builder::<String>::new();
        let binary_ops: &[(Logical, [Logical; 2], [bool; 4])] = &[
            (
                And.into(),
                [cl(false), Id.into()],
                [false, false, false, true],
            ),
            (Or.into(), [Id.into(), cl(true)], [false, true, true, true]),
            (
                Xor.into(),
                [Id.into(), Not.into()],
                [false, true, true, false],
            ),
            (
                Nor.into(),
                [Not.into(), cl(false)],
                [true, false, false, false],
            ),
            (
                Nand.into(),
                [cl(true), Not.into()],
                [true, true, true, false],
            ),
            (
                Iff.into(),
                [Not.into(), Id.into()],
                [true, false, false, true],
            ),
        ];
        for (op, partial_table, truth_table) in binary_ops.iter() {
            test_binary_operation(*op, partial_table, truth_table, &mut builder);
        }
    }
}
