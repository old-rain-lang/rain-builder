/*!
A builder for [`rain-ir`](https://gitlab.com/rain-lang/rain-ir) using the syntax defined by [`rain-ast`](https://gitlab.com/rain-lang/rain-ast).
*/
#![deny(missing_docs, unsafe_code, missing_debug_implementations)]
#![warn(clippy::all)]

use ahash::RandomState;
use hayami::{SymbolMap, SymbolTable};
use rain_ast::ast::{Expr, Ident, Statement};
use rain_ast::parser::{parse_expr, parse_statement};
use rain_ir::primitive::{
    bits::{BinOp, Bits, BitsTy},
    logical::{And, Bool, Id as LogicalId, Iff, Nand, Nor, Not, Or, Xor},
    Unit, UNIT,
};
use rain_ir::region::Region;
use rain_ir::typing::Typed;
use rain_ir::value::{self, TypeId, ValId, ValueEnum};
use std::borrow::Borrow;
use std::convert::TryInto;
use std::fmt::{self, Debug, Formatter};
use std::hash::{BuildHasher, Hash};

mod assign;
mod function;
mod gamma;
mod macros;
mod primitive;
mod scope;
mod sexpr;
mod tuple;

/// A rain IR builder
pub struct Builder<S: Hash + Eq, B: BuildHasher = RandomState> {
    /// The symbol table
    symbols: SymbolTable<S, ValId, B>,
    /// The stack of regions being defined, along with associated scope stack depth
    stack: Vec<(Region, usize)>,
}

impl<'a, S: Hash + Eq + From<&'a str>> Builder<S> {
    /// Create a new builder
    pub fn new() -> Builder<S> {
        Self::default()
    }
}

impl<'a, S: Hash + Eq + Borrow<str> + From<&'a str>, B: BuildHasher> Builder<S, B> {
    /// Build a `rain` expression into IR
    pub fn build_expr(&mut self, expr: &Expr<'a>) -> Result<ValId, Error<'a>> {
        let result_value = match expr {
            Expr::Ident(ident) => self.build_ident(*ident)?.clone(),
            Expr::Member(member) => self.build_member(member)?,
            Expr::Sexpr(sexpr) => self.build_sexpr(sexpr)?.into(),
            Expr::Tuple(tuple) => self.build_tuple(tuple)?.into(),
            Expr::Bool(b) => (*b).into(),
            Expr::BoolTy(_) => Bool.into(),
            Expr::TypeOf(ty) => self.build_typeof(ty)?.into(),
            Expr::Finite(f) => self.build_finite(*f)?.into(),
            Expr::Index(i) => self.build_index(*i)?.into(),
            Expr::Lambda(l) => self.build_lambda(l)?.into(),
            Expr::Pi(p) => self.build_pi(p)?.into(),
            Expr::Product(p) => self.build_product(p)?.into(),
            Expr::Jeq(j) => self.build_jeq(j)?.into(),
            Expr::Scope(s) => self.build_scope(s)?,
            Expr::Logical(l) => self.build_logical(*l)?.into(),
            Expr::And(_) => And.into(),
            Expr::Or(_) => Or.into(),
            Expr::Xor(_) => Xor.into(),
            Expr::Not(_) => Not.into(),
            Expr::LogicalId(_) => LogicalId.into(),
            Expr::Nand(_) => Nand.into(),
            Expr::Nor(_) => Nor.into(),
            Expr::Iff(_) => Iff.into(),
            Expr::Gamma(g) => self.build_gamma(g)?,
            Expr::Unit(_) => Unit.into(),
            Expr::Add(_) => BinOp::Add.into(),
            Expr::Sub(_) => BinOp::Sub.into(),
            Expr::Mul(_) => BinOp::Mul.into(),
            Expr::Neg(_) => return Err(Error::NotImplemented("Negation")),
            Expr::Bits(b) => Bits::try_new(BitsTy(b.len), b.data)?.into(),
        };
        Ok(result_value)
    }

    /// Build a `rain` expression into a type. Return an error if it is not
    pub fn build_ty(&mut self, expr: &Expr<'a>) -> Result<TypeId, Error<'a>> {
        self.build_expr(expr)?
            .try_into()
            .map_err(|_| Error::Message("Not a type!"))
    }

    /// Build a statement
    pub fn build_statement(&mut self, s: &Statement<'a>) -> Result<(), Error<'a>> {
        match s {
            Statement::Let(l) => self.build_let(l),
        }
    }

    /// Build a `rain` ident
    pub fn build_ident(&mut self, ident: Ident<'a>) -> Result<&ValId, Error<'a>> {
        let sym = ident
            .get_sym()
            .map_err(|_| Error::NotImplemented("Non-symbolic idents not implemented!"))?
            .ok_or(Error::Message("Cannot access the null identifier"))?;
        self.symbols.get(sym).ok_or(Error::UndefinedIdent(ident))
    }

    /// Parse an expression, and return it
    pub fn parse_expr(&mut self, expr: &'a str) -> Result<(&'a str, ValId), Error<'a>> {
        let (rest, expr) = parse_expr(expr).map_err(|_| Error::ParseError(expr))?;
        self.build_expr(&expr).map(|value| (rest, value))
    }

    /// Parse and process a statement
    pub fn parse_statement(&mut self, statement: &'a str) -> Result<&'a str, Error<'a>> {
        let (rest, statement) =
            parse_statement(statement).map_err(|_| Error::ParseError(statement))?;
        self.build_statement(&statement)?;
        Ok(rest)
    }
}

/// An error building a `rain` expression
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Error<'a> {
    /// An undefined identifier
    UndefinedIdent(Ident<'a>),
    /// An identifier which cannot be assigned to
    CannotAssignIdent(Ident<'a>),
    /// A tuple size mismatch
    TupleSizeMismatch {
        /// The tuple size obtained
        got: usize,
        /// The expected tuple size
        expected: usize,
    },
    /// Index out of bounds
    IndexOutOfBounds {
        /// The index
        ix: u128,
        /// The maximum index
        max: usize,
    },
    /// A type mismatch
    TypeMismatch {
        /// The tuple size obtained
        got: TypeId,
        /// The expected tuple size
        expected: TypeId,
    },
    /// A parse error
    ParseError(&'a str),
    /// An error message
    Message(&'a str),
    /// An unimplemented `rain` IR build
    NotImplemented(&'a str),
    /// A value error
    ValueError(&'a str, value::Error),
}

impl<'a> From<value::Error> for Error<'a> {
    fn from(error: value::Error) -> Error<'a> {
        Error::ValueError("Cast:", error)
    }
}

impl<S: Hash + Eq + Debug, B: BuildHasher> Debug for Builder<S, B> {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        fmt.debug_struct("Builder")
            .field("symbols", &self.symbols)
            .finish()
    }
}

impl<'a, S: Hash + Eq + From<&'a str>, B: BuildHasher + Default> Default for Builder<S, B> {
    fn default() -> Builder<S, B> {
        Builder {
            symbols: SymbolTable::default(),
            stack: Vec::new(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rain_ir::prettyprinter::PrettyPrint;
    use rain_ir::value::Value;

    #[test]
    fn units_build_properly() {
        let mut builder = Builder::<&str>::new();
        assert_eq!(
            builder.parse_expr("#unit").unwrap(),
            ("", ValId::from(Unit))
        );
        assert_eq!(builder.parse_expr("()").unwrap(), ("", ValId::from(())));
        assert_eq!(builder.parse_expr("[]").unwrap(), ("", ValId::from(())));
    }
    #[test]
    fn bad_indices_fail_properly() {
        let mut builder = Builder::<&str>::new();
        // Unit cannot be indexed
        assert!(builder.parse_expr("[] #ix(1)[0]").is_err());
        // Unit has no second index
        assert!(builder.parse_expr("#ix(1)[1]").is_err());
        // Empty type has no index
        assert!(builder.parse_expr("#ix(0)[0]").is_err());
        // Index out of bounds
        assert!(builder.parse_expr("[#true #false] #ix(2)[2]").is_err());
        // Index type out of bounds
        assert!(builder.parse_expr("[#true #false] #ix(3)[2]").is_err());
    }
    #[test]
    fn simple_projections_normalize_properly() {
        let mut builder = Builder::<&str>::new();
        assert_eq!(
            builder.parse_expr("[#true #false].0").unwrap(),
            ("", ValId::from(true))
        );
        assert_eq!(
            builder.parse_expr("[#true #false].1").unwrap(),
            ("", ValId::from(false))
        );
        assert_eq!(
            builder
                .parse_expr("[[#true #false] [#false #true] []].1.0")
                .unwrap(),
            ("", ValId::from(false))
        );
    }
    // use crate::primitive::logical::Bool;

    #[test]
    fn bool_identity_lambda_works_properly() {
        let mut builder = Builder::<&str>::new();
        // Build the identity
        assert_eq!(builder.parse_statement("#let id = |x: #bool| x;"), Ok(""));
        // Build the unary type
        assert_eq!(
            builder.parse_statement("#let unary = #pi|_: #bool| #bool;"),
            Ok("")
        );

        // Check dependencies and type
        let (rest, id) = builder.parse_expr("id").unwrap();
        assert_eq!(rest, "");
        assert_eq!(id.deps().len(), 0);
        let (rest, unary) = builder.parse_expr("unary").unwrap();
        assert_eq!(rest, "");
        // FIXME: this
        // assert_eq!(unary.deps().len(), 1);
        // assert_eq!(unary.deps()[0], Bool.into_val());
        assert_eq!(id.ty(), unary);

        // FIXME: this
        /*
        // Check type internally
        let (rest, jeq) = builder.parse_expr("#jeq[#typeof(id) unary]").unwrap();
        assert_eq!(rest, "");
        assert_eq!(jeq, true.into_val());
        */

        // Check evaluations
        assert_eq!(builder.parse_expr("id #true"), Ok(("", true.into_val())));
        assert_eq!(builder.parse_expr("id #false"), Ok(("", false.into_val())));

        // See if any stateful errors occur
        assert_eq!(builder.parse_expr("id #false"), Ok(("", false.into_val())));
        assert_eq!(builder.parse_expr("id #true"), Ok(("", true.into_val())));
    }

    #[test]
    fn bool_negation_lambda_builds_properly() {
        let mut builder = Builder::<&str>::new();
        // Build logical not as a lambda
        assert_eq!(
            builder.parse_statement("#let not = |x: #bool| (#not x);"),
            Ok("")
        );
        // Build the unary type
        assert_eq!(
            builder.parse_statement("#let unary = #pi|_: #bool| #bool;"),
            Ok("")
        );

        // Check dependencies and type externally
        let (rest, not) = builder.parse_expr("not").unwrap();
        assert_eq!(rest, "");
        let (rest, unary) = builder.parse_expr("unary").unwrap();
        assert_eq!(rest, "");
        assert_eq!(not.ty(), unary);

        // FIXME: this
        /*
        // Check depdendencies and types internally
        let (rest, jeq) = builder.parse_expr("#jeq[#typeof(not) unary]").unwrap();
        assert_eq!(rest, "");
        assert_eq!(jeq, true.into_val());
        */

        // Check evaluations
        assert_eq!(builder.parse_expr("not #true"), Ok(("", false.into_val())));
        assert_eq!(builder.parse_expr("not #false"), Ok(("", true.into_val())));

        // See if any stateful errors occur
        assert_eq!(builder.parse_expr("not #false"), Ok(("", true.into_val())));
        assert_eq!(builder.parse_expr("not #true"), Ok(("", false.into_val())));
    }

    #[test]
    fn mux_lambda_builds_properly() {
        // Get mux evaluation programs
        let programs: Vec<_> = (0b000..=0b111)
            .map(|v| {
                let select = v & 0b100 != 0;
                let high = v & 0b010 != 0;
                let low = v & 0b001 != 0;
                (
                    format!("mux {} {} {}", select.prp(), high.prp(), low.prp()),
                    if select { high } else { low },
                )
            })
            .collect();

        let mut builder = Builder::<&str>::new();
        // Build mux as a lambda
        let mux_program =
            "#let mux = |select: #bool high: #bool low: #bool| (#or (#and select high) (#and (#not select) low));";
        assert_eq!(builder.parse_statement(mux_program), Ok(""));
        // Build the ternary type
        assert_eq!(
            builder.parse_statement("#let ternary = #pi|_: #bool _: #bool _: #bool| #bool;"),
            Ok("")
        );

        // Check dependencies and type externally
        let (rest, mux) = builder.parse_expr("mux").unwrap();
        assert_eq!(rest, "");
        // FIXME: this
        // assert_eq!(mux.deps().len(), 3); // default-features = "false"and, or, not
        let (rest, ternary) = builder.parse_expr("ternary").unwrap();
        assert_eq!(rest, "");
        // FIXME: this
        // assert_eq!(ternary.deps().len(), 1);
        // assert_eq!(ternary.deps()[0], Bool.into_val());
        assert_eq!(mux.ty(), ternary);

        // FIXME: this
        /*
        // Check depdendencies and types internally
        let (rest, jeq) = builder.parse_expr("#jeq[#typeof(mux) ternary]").unwrap();
        assert_eq!(rest, "");
        assert_eq!(jeq, true.into_val());
        */

        // Compute evaluations
        for (program, desired_result) in programs.iter() {
            let (rest, result) = builder.parse_expr(program).unwrap();
            assert_eq!(rest, "");
            assert_eq!(result, desired_result.into_val());
        }
    }
}
