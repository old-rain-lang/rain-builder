/*!
Build tuples and product types
*/

use super::*;
use num::ToPrimitive;
use rain_ast::ast::{Member, Product as ProductExpr, Tuple as TupleExpr};
use rain_ir::primitive::finite::{Finite, Index};
use rain_ir::value::{
    expr::Sexpr,
    tuple::{Product, Tuple},
};

impl<'a, S: Hash + Eq + Borrow<str> + From<&'a str>, B: BuildHasher> Builder<S, B> {
    /// Build a member expression
    pub fn build_member(&mut self, member: &Member<'a>) -> Result<ValId, Error<'a>> {
        let mut base = self.build_expr(&member.base)?;
        for ident in member.path.iter() {
            if let Ok(ix) = ident.get_u128() {
                // First try direct tuple indexing
                match base.as_enum() {
                    ValueEnum::Tuple(t) => {
                        let ix = if let Some(ix_u) = ix.to_usize() {
                            if ix_u < t.len() {
                                ix_u
                            } else {
                                return Err(Error::IndexOutOfBounds { ix, max: t.len() });
                            }
                        } else {
                            return Err(Error::IndexOutOfBounds { ix, max: t.len() });
                        };
                        base = t[ix].clone();
                    }
                    _ => match base.ty().as_enum() {
                        // Else try index-expression building
                        ValueEnum::Product(p) => {
                            base = Sexpr::try_new(vec![
                                base.clone(),
                                Index::try_new(Finite(p.len() as u128), ix)
                                    .map_err(|_| Error::IndexOutOfBounds { ix, max: p.len() })?
                                    .into(),
                            ])?
                            .into();
                        }
                        _ => return Err(Error::Message("Non-tuple indexing not yet implemented!")),
                    },
                }
            } else {
                return Err(Error::Message(
                    "Non-numeric member-access not yet implemented!",
                ));
            }
        }
        Ok(base)
    }

    /// Build a tuple
    pub fn build_tuple(&mut self, tuple: &TupleExpr<'a>) -> Result<Tuple, Error<'a>> {
        let elems: Result<_, _> = tuple.0.iter().map(|elem| self.build_expr(elem)).collect();
        Tuple::try_new(elems?).map_err(|_| Error::Message("Failed to build tuple"))
    }

    /// Build a product type
    pub fn build_product(&mut self, product: &ProductExpr<'a>) -> Result<Product, Error<'a>> {
        let elems: Result<_, _> = product.0.iter().map(|elem| self.build_ty(elem)).collect();
        Product::try_new(elems?).map_err(|_| Error::Message("Failed to build product"))
    }
}
