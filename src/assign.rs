/*!
Build assignment statements
*/

use super::*;
use rain_ast::ast::{Assign, Detuple, Simple};

impl<'a, S: Hash + Eq + Borrow<str> + From<&'a str>, B: BuildHasher> Builder<S, B> {
    /// Build an assignment
    pub fn build_assign(&mut self, p: &Assign<'a>, v: ValId) -> Result<(), Error<'a>> {
        match p {
            Assign::Simple(s) => self.build_simple(s, v),
            Assign::Detuple(d) => self.build_detuple(d, v),
        }
    }
    /// Build a simple assignment
    pub fn build_simple(&mut self, s: &Simple<'a>, v: ValId) -> Result<(), Error<'a>> {
        if let Some(_ty) = s.ty.as_ref() {
            return Err(Error::NotImplemented(
                "Simple-assignment type checking not yet implemented!",
            ));
        }
        if let Some(var) = s.var.get_sym().map_err(Error::CannotAssignIdent)? {
            //TODO: pattern-assignment
            self.symbols.insert(var.into(), v);
        }
        Ok(())
    }

    /// Build a tuple-destructure assignment
    pub fn build_detuple(&mut self, d: &Detuple<'a>, v: ValId) -> Result<(), Error<'a>> {
        match d.0.len() {
            0 => {
                let got = v.ty();
                if got == UNIT.borrow_var() {
                    Ok(())
                } else {
                    Err(Error::TypeMismatch {
                        got: got.clone_ty(),
                        expected: Unit.into(),
                    })
                }
            }
            _ => Err(Error::NotImplemented(
                "Non-unit tuple destructure is not yet implemented!",
            )),
        }
    }
}
